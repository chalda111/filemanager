package test.playment.com.filemanager.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import test.playment.com.filemanager.data.DirectoryContract;
import test.playment.com.filemanager.data.DirectoryDbHelper;

public class DirectoryProvider extends ContentProvider {

    private static final int URI_TYPE_DIRECTORY = 1;
    private static final int URI_TYPE_DIRECTORY_ITEM = 2;

    private final String UNSUPPORTED_URI_TEXT = "Unknown uri: ";
    private final String FAILED_ERROR_TEXT = "Operation unsuccessfull: ";

    private DirectoryDbHelper mDbHelper;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(DirectoryContract.CONTENT_AUTHORITY,
                DirectoryContract.PATH_DIRECTORY, URI_TYPE_DIRECTORY);
        sUriMatcher.addURI(DirectoryContract.CONTENT_AUTHORITY,
                DirectoryContract.PATH_DIRECTORY + "/#", URI_TYPE_DIRECTORY_ITEM);
    }


    @Override
    public boolean onCreate() {
        mDbHelper = DirectoryDbHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor;
        switch (sUriMatcher.match(uri)) {
            case URI_TYPE_DIRECTORY:
                cursor = db.query(DirectoryContract.DirectoryEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case URI_TYPE_DIRECTORY_ITEM:
                selection = DirectoryContract.DirectoryEntry._ID + " = ?";
                selectionArgs = new String[] {uri.getLastPathSegment()};
                cursor = db.query(DirectoryContract.DirectoryEntry.TABLE_NAME, null,
                        selection, selectionArgs, null, null, null);
                break;
            default:
                throw new UnsupportedOperationException(UNSUPPORTED_URI_TEXT + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case URI_TYPE_DIRECTORY:
                return DirectoryContract.DirectoryEntry.CONTENT_TYPE;
            case URI_TYPE_DIRECTORY_ITEM:
                return DirectoryContract.DirectoryEntry.CONTENT_TYPE_ITEM;
            default:
                throw new UnsupportedOperationException(UNSUPPORTED_URI_TEXT + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Uri retUri;

        switch (sUriMatcher.match(uri)) {
            case URI_TYPE_DIRECTORY:
                long id = db.insert(DirectoryContract.DirectoryEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    retUri = DirectoryContract.DirectoryEntry.buildDirectoryUriForId(id);
                } else {
                    throw new android.database.SQLException(FAILED_ERROR_TEXT + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException(UNSUPPORTED_URI_TEXT + uri);

        }
        getContext().getContentResolver().notifyChange(uri, null);
        return retUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        //TODO: Not required as of now, do later if needed
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values,
                      @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        //TODO: Not required as of now, do later if needed
        return 0;
    }
}
