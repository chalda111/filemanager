package test.playment.com.filemanager.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import test.playment.com.filemanager.R;
import test.playment.com.filemanager.data.DirectoryContract;

public class DirectoryAdapter extends RecyclerView.Adapter {

    private Cursor mCursor;
    private Context mContext;
    private OnAdapterItemClickListener mOnAdapterItemClickListener;

    public DirectoryAdapter(Context context, Cursor cursor) {
        super();
        mCursor = cursor;
        mContext = context;
        mOnAdapterItemClickListener = (OnAdapterItemClickListener) context;
    }

    @Override
    public DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_directory, parent, false);
        return new DirectoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DirectoryViewHolder directoryViewHolder = (DirectoryViewHolder) holder;
        directoryViewHolder.mDirectoryNameTv.setText(getDirectoryName(position));
    }

    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public class DirectoryViewHolder extends RecyclerView.ViewHolder {

        private TextView mDirectoryNameTv;

        public DirectoryViewHolder(View itemView) {
            super(itemView);
            mDirectoryNameTv = (TextView) itemView.findViewById(R.id.tv_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long id = getDirectoryId(getAdapterPosition());
                    mOnAdapterItemClickListener.onItemClick(id);
                }
            });
        }

    }

    private String getDirectoryName(int position) {
        mCursor.moveToPosition(position);
        return mCursor.getString(
                mCursor.getColumnIndex(DirectoryContract.DirectoryEntry.COLUMN_NAME));
    }

    private long getDirectoryId(int position) {
        mCursor.moveToPosition(position);
        return mCursor.getLong(
                mCursor.getColumnIndex(DirectoryContract.DirectoryEntry._ID));
    }

    public void setNewCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
    }

    public interface OnAdapterItemClickListener {
        void onItemClick(long id);
    }
 }
