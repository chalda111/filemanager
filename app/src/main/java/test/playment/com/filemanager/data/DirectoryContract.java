package test.playment.com.filemanager.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public final class DirectoryContract {

    private DirectoryContract (){}

    public static final String CONTENT_AUTHORITY = "test.playment.com.filemanager";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_DIRECTORY = "directory";

    private static final String SEPARATOR = "/";

    public static final class DirectoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI
                = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DIRECTORY).build();

        public static final String CONTENT_TYPE
                = ContentResolver.CURSOR_DIR_BASE_TYPE + SEPARATOR
                + CONTENT_AUTHORITY + SEPARATOR + PATH_DIRECTORY;

        public static final String CONTENT_TYPE_ITEM
                = ContentResolver.CURSOR_ITEM_BASE_TYPE + SEPARATOR
                + CONTENT_AUTHORITY + SEPARATOR + PATH_DIRECTORY;

        public static final String TABLE_NAME = PATH_DIRECTORY;
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_PARENT_ID = "parent_id";

        public static Uri buildDirectoryUriForId(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static final String CREATE_TABLE_DIRECTORY = "CREATE TABLE "
                + DirectoryEntry.TABLE_NAME + " (" + DirectoryEntry._ID + " INTEGER PRIMARY KEY, "
                + DirectoryEntry.COLUMN_NAME + " TEXT, "
                + DirectoryEntry.COLUMN_PARENT_ID + " INTEGER )";


    }
}
