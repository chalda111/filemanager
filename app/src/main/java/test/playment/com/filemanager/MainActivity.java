package test.playment.com.filemanager;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import test.playment.com.filemanager.adapter.DirectoryAdapter;
import test.playment.com.filemanager.data.DirectoryContract;
import test.playment.com.filemanager.util.DirectoryQueryHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor>, DirectoryAdapter.OnAdapterItemClickListener {

    private final int LOAD_DIRECTORIES = 100;
    private final int GET_PARENT = 200;
    private final String CURRENT_DIRECTORY = "current_directory";

    private View mAddDirectoryBtn;
    private DirectoryQueryHelper mQueryHelper;
    private RecyclerView mDirectoryListRv;
    private View mEmptyView;
    private DirectoryAdapter mDirectoryAdapter;
    private long mCurrentDirectoryId = -1;
    private boolean mIsBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(savedInstanceState != null) {
            mCurrentDirectoryId = savedInstanceState.getLong(CURRENT_DIRECTORY);
        }

        mAddDirectoryBtn = findViewById(R.id.btn_add_new);
        mAddDirectoryBtn.setOnClickListener(this);
        mQueryHelper = new DirectoryQueryHelper(this);
        mDirectoryListRv = (RecyclerView) findViewById(R.id.rv_directory);
        mDirectoryListRv.setLayoutManager(new LinearLayoutManager(this));
        mDirectoryListRv.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mEmptyView = findViewById(R.id.tv_empty);
        getSupportLoaderManager().initLoader(LOAD_DIRECTORIES, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch(id){
            case LOAD_DIRECTORIES:
                String selection = DirectoryContract.DirectoryEntry.COLUMN_PARENT_ID + " = ?";
                String[] selectionArgs = new String[] {String.valueOf(mCurrentDirectoryId)};
                return new CursorLoader(this, DirectoryContract.DirectoryEntry.CONTENT_URI, null,
                        selection, selectionArgs,
                        DirectoryContract.DirectoryEntry.COLUMN_NAME + " ASC");
            case GET_PARENT:
                return new CursorLoader(this,
                        DirectoryContract.DirectoryEntry.buildDirectoryUriForId(mCurrentDirectoryId),
                        null, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()){
            case LOAD_DIRECTORIES:
                mIsBackPressed = false;
                if(data == null || data.getCount() == 0) {
                    mDirectoryListRv.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                    return;
                } else {
                    mEmptyView.setVisibility(View.GONE);
                    mDirectoryListRv.setVisibility(View.VISIBLE);
                    if (mDirectoryAdapter == null) {
                        mDirectoryAdapter = new DirectoryAdapter(MainActivity.this, data);
                        mDirectoryListRv.setAdapter(mDirectoryAdapter);
                    } else {
                        mDirectoryAdapter.setNewCursor(data);
                    }
                }
                break;
            case GET_PARENT:
                if(data != null){
                    data.moveToFirst();
                    mCurrentDirectoryId = data.getLong(
                            data.getColumnIndex(DirectoryContract.DirectoryEntry.COLUMN_PARENT_ID));
                    getSupportLoaderManager().restartLoader(LOAD_DIRECTORIES, null,
                            MainActivity.this);
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mDirectoryAdapter.setNewCursor(null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(CURRENT_DIRECTORY, mCurrentDirectoryId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_new:
                showAlertDialog();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if(!mIsBackPressed) {
            mIsBackPressed = true;
            if (mCurrentDirectoryId != -1) {
                if (getSupportLoaderManager().getLoader(GET_PARENT) == null) {
                    getSupportLoaderManager().initLoader(GET_PARENT, null, this);
                } else {
                    getSupportLoaderManager().restartLoader(GET_PARENT, null, this);
                }
                return;
            }
            super.onBackPressed();
        }
    }

    @Override
    public void onItemClick(long id) {
        mCurrentDirectoryId = id;
        getSupportLoaderManager().restartLoader(LOAD_DIRECTORIES, null, this);
    }

    private void showAlertDialog() {
        final EditText input = new EditText(MainActivity.this);
        input.setSingleLine();
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setView(input)
                .setTitle(getString(R.string.ask_name_text))
                .setPositiveButton(getString(R.string.btn_text_ok), null)
                .setNegativeButton(getString(R.string.btn_text_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(TextUtils.isEmpty(input.getText().toString())){
                                    input.setError(getString(R.string.error_input_empty));
                                    return;
                                }
                                mQueryHelper.addNewDirectory(input.getText().toString(),
                                        mCurrentDirectoryId);
                                dialog.dismiss();
                            }
                        });
            }
        });
        alertDialog.show();
    }
}
