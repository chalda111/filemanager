package test.playment.com.filemanager.util;


import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import test.playment.com.filemanager.data.DirectoryContract;

public class DirectoryQueryHelper {

    private final int TOKEN_INSERT = 50;

    private AsyncQueryHandler mAsyncQueryHandler;

    public DirectoryQueryHelper(Context context) {
        mAsyncQueryHandler = new AsyncQueryHandler(context.getContentResolver()) {
            @Override
            protected void onInsertComplete(int token, Object cookie, Uri uri) {
                super.onInsertComplete(token, cookie, uri);
            }
        };
    }

    public void addNewDirectory(String name, long parent) {
        ContentValues values = new ContentValues();
        values.put(DirectoryContract.DirectoryEntry.COLUMN_NAME, name);
        values.put(DirectoryContract.DirectoryEntry.COLUMN_PARENT_ID, parent);
        mAsyncQueryHandler.startInsert(TOKEN_INSERT, false,
                DirectoryContract.DirectoryEntry.CONTENT_URI, values);
    }

}
