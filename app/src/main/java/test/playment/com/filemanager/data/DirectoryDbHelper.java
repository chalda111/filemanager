package test.playment.com.filemanager.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DirectoryDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "directory.db";

    private static DirectoryDbHelper sInstance;

    public static synchronized DirectoryDbHelper getInstance(Context context) {
        if(sInstance == null) {
            sInstance = new DirectoryDbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private DirectoryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DirectoryContract.DirectoryEntry.CREATE_TABLE_DIRECTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /* TODO:// Not required for now, add later as per requirement */
    }
}
